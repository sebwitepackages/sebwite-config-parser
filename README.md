Sebwite Config-parser
====================

[![Build Status](https://img.shields.io/travis/sebwite/config-parser.svg?&style=flat-square)](https://travis-ci.org/sebwite/config-parser)
[![Scrutinizer coverage](https://img.shields.io/scrutinizer/coverage/g/sebwite/config-parser.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/config-parser)
[![Scrutinizer quality](https://img.shields.io/scrutinizer/g/sebwite/config-parser.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/config-parser)
[![Source](http://img.shields.io/badge/source-sebwite/config-parser-blue.svg?style=flat-square)](https://github.com/sebwite/config-parser)
[![License](http://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](http://mit-license.org)

Sebwite Config-parser is a package for the Laravel 5 framework.

The package follows the FIG standards PSR-1, PSR-2, and PSR-4 to ensure a high level of interoperability between shared PHP code.

Documentation
-------------
Tbd

Quick Installation
------------------
Begin by installing the package through Composer.

```bash
composer require sebwite/config-parser
```

Add the bootstrapper to both `Console` and `Http` kernels
```php
class Kernel extends ConsoleKernel
{
    protected $bootstrappers = [
        'Illuminate\Foundation\Bootstrap\DetectEnvironment',
        'Illuminate\Foundation\Bootstrap\LoadConfiguration',
        'Sebwite\ConfigParser\DecorateConfiguration',
        'Illuminate\Foundation\Bootstrap\ConfigureLogging',
        'Illuminate\Foundation\Bootstrap\HandleExceptions',
        'Illuminate\Foundation\Bootstrap\RegisterFacades',
        'Illuminate\Foundation\Bootstrap\SetRequestForConsole',
        'Illuminate\Foundation\Bootstrap\RegisterProviders',
        'Illuminate\Foundation\Bootstrap\BootProviders',
    ];
}
```

