<?php

namespace Sebwite\Tests\ConfigParser;

abstract class TestCase extends \Sebwite\Testbench\TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function getServiceProviderClass()
    {
        return \Sebwite\ConfigParser\ConfigParserServiceProvider::class;
    }

   /**
    * {@inheritdoc}
    */
    protected function getPackageRootPath()
    {
        return __DIR__ . DIRECTORY_SEPARATOR . '..';
    }
}
