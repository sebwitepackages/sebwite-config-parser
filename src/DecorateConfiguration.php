<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\ConfigParser;

use Illuminate\Contracts\Foundation\Application;



class DecorateConfiguration
{
    public function bootstrap(Application $app)
    {

        $config = $app->make('config');
        $config = ConfigRepositoryDecorator::create($config);
        $app->instance('config', $config);
    }
}


