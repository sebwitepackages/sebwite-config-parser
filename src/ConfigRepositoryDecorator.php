<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\ConfigParser;

use ArrayAccess;
use Illuminate\Contracts\Config\Repository as RepositoryContract;
use Illuminate\View\Compilers\BladeCompiler;
use Sebwite\Support\StubGenerator;

class ConfigRepositoryDecorator implements ArrayAccess, RepositoryContract
{
    protected $config;

    protected $generator;

    public function __construct(StubGenerator $generator, RepositoryContract $config)
    {
        $this->config    = $config;
        $this->generator = $generator;
    }

    public static function create(RepositoryContract $config)
    {
        $blade = new BladeCompiler(new \Illuminate\Filesystem\Filesystem, storage_path('config-parser'));
        $generator = new StubGenerator($blade, new \Sebwite\Support\Filesystem);
        return new static($generator, $config);
    }

    protected function isParsableValue($value)
    {
        if ($value === null) {
            return false;
        } elseif (is_array($value)) {
            return true;
        }

        return preg_match('/\{\{([\w\W]*?)\}\}/', $value) > 0;
    }

    protected function parseValue(&$value)
    {
        if (is_array($value)) {
            foreach ($value as $key => &$item) {
                $item = $this->parseValue($item);
            }

            return $value;
        } else {
            return $this->generator->render($value, [
                'config' => $this
            ]);
        }
    }

    public function getRaw($key, $default = null)
    {
        return $this->config->get($key, null);
    }

    public function allRaw()
    {
        return $this->config->all();
    }

    public function get($key, $default = null)
    {
        $value = $this->config->get($key, null);
        if ($this->isParsableValue($value)) {
        // parse lol
            return $this->parseValue($value);
        } else {
            // @todo requesting the value 2 times? sort it out
            return $this->config->get($key, $default);
        }
    }

    public function offsetExists($offset)
    {
        return $this->config->has($offset);
    }

    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function offsetSet($offset, $value)
    {
        $this->config->set($offset, $value);
    }

    public function offsetUnset($offset)
    {
        $this->config->set($offset, null);
    }

    /**
     * Determine if the given configuration value exists.
     *
     * @param  string $key
     *
     * @return bool
     */
    public function has($key)
    {
        return $this->config->has($key);
    }

    /**
     * Get all of the configuration items for the application.
     *
     * @return array
     */
    public function all()
    {
        $all = [];
        foreach ($this->config->all() as $key => $value) {
            $all[$key] = $this->get($key);
        }
        return $all;
    }

    /**
     * Set a given configuration value.
     *
     * @param  array|string $key
     * @param  mixed        $value
     *
     * @return void
     */
    public function set($key, $value = null)
    {
        $this->config->set($key, $value);
    }

    /**
     * Prepend a value onto an array configuration value.
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @return void
     */
    public function prepend($key, $value)
    {
        $this->config->prepend($key, $value);
    }

    /**
     * Push a value onto an array configuration value.
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @return void
     */
    public function push($key, $value)
    {
        $this->config->push($key, $value);
    }

    public function __call($method, array $params = [ ])
    {
        if (method_exists($this->config, $method)) {
            return call_user_func_array([ $this->config, $method ], $params);
        }
        throw new \BadMethodCallException("Cannot execute [{$method}] from class Parser or any of its magical children");
    }
}
